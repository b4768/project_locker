const model = require('../models')

module.exports = {
    list: async (req, res) => {
        try {
            const datas = await model.locker_type.findAll()

            return res.status(200).json({
                'success' : true,
                'error' : 0,
                'message' : 'Data Successfully listed',
                'data' : datas
            })
        } catch (error) {
            return res.status(500).json({
                'success' : false,
                'error' : error.code,
                'message' : error,
                'data' : null
            })
        }
    },
    create: async (req, res) => {
        try {
            const data = await model.locker_type.create(req.body)

            return res.status(200).json({
                'success' : true,
                'error' : 0,
                'message' : 'Data Successfully create',
                'data' : data
            })
        } catch (error) {
            return res.status(500).json({
                'success' : false,
                'error' : error.code,
                'message' : error,
                'data' : null
            })
        }
    },
    update: async (req, res) => {
        try {
            const data = await model.locker_type.update({
                code : req.body.code,
                size : req.body.size,
                price : req.body.price,
                location : req.body.location,
                quantity : req.body.quantity
            }, {
                where: {
                    id: req.body.id
                }
            })

            return res.status(200).json({
                'success' : true,
                'error' : 0,
                'message' : 'Data Successfully update',
                'data' : data
            })
        } catch (error) {
            return res.status(500).json({
                'success' : false,
                'error' : error.code,
                'message' : error,
                'data' : null
            })
        }
    },
    destroy: async (req, res) => {
        try {
            const data = await model.locker_type.destroy({
                where: {
                    id: req.body.id
                }
            })

            return res.status(200).json({
                'success' : true,
                'error' : 0,
                'message' : 'Data Successfully delete',
                'data' : data
            })
        } catch (error) {
            return res.status(500).json({
                'success' : false,
                'error' : error.code,
                'message' : error,
                'data' : null
            })
        }
    }
}