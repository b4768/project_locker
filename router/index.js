const express = require("express");
const userRouter = require("./user")
const locker_typeRouter = require("./locker_type")
const router = express.Router();
//add new require's here

router.get("/check-healty", (req, res) => res.send("aplication up"));
router.use("/user", userRouter)
router.use("/locker_type", locker_typeRouter)

module.exports = router;
