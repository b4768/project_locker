const express = require('express')
const router = express.Router()
const { list, create, update, destroy } = require('../controllers/locker_typeController.js')
const validate = require('../middleware/validate')

router.get('/list', list)
router.post('/create', create)
router.put('/update', update)
router.delete('/delete', destroy)

module.exports = router