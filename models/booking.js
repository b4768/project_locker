"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class booking extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  booking.init(
    {
      user_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      looker_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      booking_start: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      booking_end: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      num_lokers: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: "booking",
    }
  );
  return booking;
};
