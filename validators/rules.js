const { body } = require('express-validator')

const createUserRules = [
    body('email').isEmail().withMessage('email invalid').notEmpty().withMessage('email is required'),
    body('name').notEmpty().withMessage('username is required'),
    body('password').notEmpty().withMessage('password is required')
]

module.exports = {
    createUserRules
}